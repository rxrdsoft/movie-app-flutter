import 'package:flutter/material.dart';
import 'package:movie_app/src/models/pelicula_model.dart';
import 'package:movie_app/src/providers/peliculas_provider.dart';

class DataSearch extends SearchDelegate {

  final peliculasProvider = PeliculasProvider();
  final peliculas = [
    'Spidermen',
    'Aquaman',
    'Batman',
    'Enola',
    'Juegos del hambre'
  ];

  final peliculasRecientes = [
    'Spidermen',
    'Iron man',
    'Sherk',
    'Tron'
  ];


  @override
  List<Widget> buildActions(BuildContext context) {
    // Acciones de nuestro app bar
    return [
      IconButton(icon: Icon(Icons.close), onPressed: (){
        print('click clean');
        query = '';
      })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la izquierda del app bar
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            progress: transitionAnimation),
        onPressed: (){
          print('Leading icon press');
          close(context, null);
        }
        );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Crea los resultados que vamos a mostrar
   return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Sugerencias que aparecen cuando la persona escribe
    if(query.isEmpty){
      return Container();
    }

    return FutureBuilder(
        future: peliculasProvider.buscarPeliculas(query),
        builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
          if(snapshot.hasData) {
            final peliculas = snapshot.data;
            return ListView(
              children: peliculas.map((pelicula) {
                return ListTile(
                  leading: FadeInImage(
                    placeholder: AssetImage('assets/img/loading.gif'),
                    image: NetworkImage(pelicula.getPosterImg()),
                    fit: BoxFit.contain,
                  ),
                  title: Text(pelicula.title),
                  subtitle: Text(pelicula.originalTitle),
                  onTap: (){
                    close(context, null);
                    pelicula.uniqueId = '';
                    Navigator.pushNamed(context, '/detalle',arguments: pelicula);
                  },
                );
              }).toList(),
            );
          }else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
      }
    );
  }

  // @override
  // Widget buildSuggestions(BuildContext context) {
  //   // Sugerencias que aparecen cuando la persona escribe
  //
  //   final listaSugerida = query.isEmpty
  //       ? peliculasRecientes
  //       : peliculas.where(
  //           (p) => p.toLowerCase().startsWith(query.toLowerCase()
  //           )
  //   ).toList();
  //
  //   return ListView.builder(
  //       itemCount: listaSugerida.length,
  //       itemBuilder: (context,i) {
  //         return ListTile(
  //           leading: Icon(Icons.movie),
  //           title: Text(listaSugerida[i]),
  //         );
  //       }
  //   );
  // }


}