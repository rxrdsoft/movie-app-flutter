import 'package:flutter/material.dart';
import 'package:movie_app/src/models/pelicula_model.dart';

class MovieHorizontal extends StatelessWidget {
  final List<Pelicula> peliculas;
  final Function sigueintePagina;
  final _pageController = PageController(
      initialPage: 1,
      viewportFraction: 0.3
  );

  MovieHorizontal({@required this.peliculas, @required this.sigueintePagina});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    _pageController.addListener(() {
      if(_pageController.position.pixels >= _pageController.position.maxScrollExtent -200){
        sigueintePagina();
      }
    });

    return Container(
      height: _screenSize.height*0.2,
      child: PageView.builder(
          itemCount: peliculas.length,
          pageSnapping: false,
          controller: _pageController,
          itemBuilder: (context,i) => _tarjeta(context,peliculas[i])
          // children: _tarjetas(context),
      ),
    );
  }

  Widget _tarjeta(BuildContext context,Pelicula pelicula) {
    pelicula.uniqueId = '${pelicula.id}-poster';
    final tarjeta =  Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: [
          Hero(
            tag: pelicula.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/loading.gif'),
                image: NetworkImage(pelicula.getPosterImg()),
                fit: BoxFit.cover ,
                height: 140.0,
              ),
            ),
          ),
          SizedBox(height: 2.0),
          Text('${pelicula.title}',
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );

    return GestureDetector(
      child: tarjeta,
      onTap: () {
        print('click en movidde: ${pelicula.title}');
        Navigator.pushNamed(context, '/detalle',arguments: pelicula);
      },
    );
  }

  List<Widget> _tarjetas(context) {
    return peliculas.map((pelicula){
      return Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/loading.gif'),
                image: NetworkImage(pelicula.getPosterImg()),
                fit: BoxFit.cover ,
                height: 140.0,
              ),
            ),
            SizedBox(height: 2.0),
            Text('${pelicula.title}',
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      );
    }).toList();
  }
}
