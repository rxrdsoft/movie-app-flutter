import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/src/models/pelicula_model.dart';
import 'package:movie_app/src/providers/peliculas_provider.dart';
import 'package:movie_app/src/seasrch/search_delegate.dart';
import 'package:movie_app/src/widgets/card_swiper_widget.dart';
import 'package:movie_app/src/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {
  final peliculasProvider = PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    peliculasProvider.getPopulares();

    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Diseño #1'),
              onTap: (){
                Navigator.pushNamed(context, '/design-one');
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Peliculas de cines'),
        backgroundColor: Colors.indigoAccent,
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {
            showSearch(
                context: context,
                delegate: DataSearch(),
                // query: 'Hola'
            );
          })
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _swiperTarjetas(),
            _footer(context)
          ],
        ),
      ),
    );
  }

  Widget _swiperTarjetas() {
    return FutureBuilder(
        future: peliculasProvider.getEnCineas(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return CardSwiper(peliculas: snapshot.data);
          }
          return Container(
              height: 400.0,
              child: Center(
                  child: CircularProgressIndicator()
              )
          );
          }

    );
  }

  Widget _footer(BuildContext context){
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 20.0),
            child: Text('Populares',
                style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          SizedBox(height: 5.0,),
          StreamBuilder(
          stream: peliculasProvider.popularStram,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot){
            if(snapshot.hasData) {
              return MovieHorizontal(peliculas: snapshot.data, sigueintePagina: peliculasProvider.getPopulares,);
            }else {
              return Center(child: CircularProgressIndicator());
            }
          }
          ),
        ],
      ),
    );
  }
}
