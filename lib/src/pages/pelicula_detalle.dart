import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/src/models/actores_model.dart';
import 'package:movie_app/src/models/pelicula_model.dart';
import 'package:movie_app/src/providers/actores_provider.dart';

class PeliculaDetalle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          _crearAppBar(pelicula),
          SliverList(
              delegate: SliverChildListDelegate(
                  [
                    SizedBox(height: 10.0),
                    _posterTitulo(pelicula),
                    _descripcion(pelicula),
                    _descripcion(pelicula),
                    _descripcion(pelicula),
                    _descripcion(pelicula),
                    _descripcion(pelicula),
                    _descripcion(pelicula),
                    _crearActores(pelicula)
                  ]
              )
          )
        ],
      ),
    );
  }

  Widget _crearActores(Pelicula pelicula){
    final actoresProvider = ActoresProvider();
    return FutureBuilder(
        future: actoresProvider.getActores(pelicula.id),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if(snapshot.hasData) {
            return _crearActorePageView(snapshot.data);
          }else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }
    );
  }
  Widget _crearActorePageView(List<Actor> actores) {
    return Container(
      height: 200.0,
      child: PageView.builder(
          pageSnapping: false,
          controller: PageController(
            viewportFraction: 0.3,
            initialPage: 1
          ) ,
          itemCount: actores.length,
          itemBuilder: (context,i) {
            return _actorTarjeta(context,actores[i]);
          }
      ),
    );
  }

  Widget _actorTarjeta(context,Actor actor) {
    return Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              placeholder: AssetImage('assets/img/loading.gif'),
              image: NetworkImage(actor.getFoto()),
              fit: BoxFit.cover ,
              height: 140.0,
            ),
          ),
          SizedBox(height: 2.0),
          Text('${actor.name}',
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
  }

  Widget _descripcion(Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 20.0),
      child: Text(pelicula.overview,
      textAlign: TextAlign.justify,),
    );
  }

  Widget _posterTitulo(Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: [
          Hero(
            tag: pelicula.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                  image: NetworkImage(pelicula.getPosterImg()),
                height: 150.0,
              ),
            ),
          ),
          SizedBox(width: 20.0),
          Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(pelicula.title,style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    pelicula.originalTitle,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    children: [
                      Icon(Icons.star_border),
                      Text(pelicula.voteAverage.toString())
                    ],
                  )
                ],
              )
          )
        ],
      ),
    );
  }

  Widget _crearAppBar(Pelicula pelicula) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(pelicula.title),
        background: FadeInImage(
            placeholder: AssetImage('assets/img/loading.gif'),
            image: NetworkImage(pelicula.getBackgroundImg()),
            fit: BoxFit.cover,
        ),
      ),
    );
  }
}
