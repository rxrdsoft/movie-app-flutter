import 'package:flutter/material.dart';


class DesignOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            FadeInImage(
                placeholder: AssetImage('assets/img/loading.gif'),
                image: NetworkImage('https://resi.ze-robot.com/dl/ul/ultraviolet-4k-wallpaper-3840%C3%972160.jpg'),
                height: 250.0 ,
                fit: BoxFit.cover ,
            ),
            SizedBox(height: 20.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              width: double.infinity,
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Laguna de Pomacochas',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold
                          )
                          ),
                        SizedBox(height: 8.0,),
                        Text('Florida - pomacochas')
                      ],
                    ),
                  ),
                  Icon(Icons.star,color: Colors.red,),
                  Text('41')
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Icon(Icons.phone),
                    Text('Phone')
                  ],
                ),
                Column(
                  children: [
                    Icon(Icons.phone),
                    Text('Phone')
                  ],
                ),
                Column(
                  children: [
                    Icon(Icons.phone),
                    Text('Phone')
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
