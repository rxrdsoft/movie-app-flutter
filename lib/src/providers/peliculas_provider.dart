import 'dart:async';
import 'dart:convert';

import 'package:movie_app/src/models/pelicula_model.dart';
import 'package:http/http.dart' as http;

class PeliculasProvider{
  String _apiKey = '27e43c48a67719f0f5211724f785b4cd';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';
  int _popularesPage = 0;
  bool _cargando = false;

  List<Pelicula> _populares = List();

  final _popularesStremController = StreamController<List<Pelicula>>.broadcast();


  Function(List<Pelicula>) get popularesSink => _popularesStremController.sink.add;

  Stream<List<Pelicula>> get popularStram => _popularesStremController.stream;

  void disposeStreams() {
    _popularesStremController?.close();
  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async {
    final response = await http.get(url);
    final decodedData = jsonDecode(response.body);
    final peliculas = Peliculas.fromJsonList(decodedData['results']);
    return peliculas.items;
  }

  Future<List<Pelicula>> getEnCineas() async {
      final url = Uri.https(_url, '3/movie/now_playing',{
        'api_key': _apiKey,
        'language': _language
      });

      return await _procesarRespuesta(url);
  }
  Future<List<Pelicula>> getPopulares() async {
    if(_cargando) return [];
    print('cargando peliculas');
    _cargando = true;

    _popularesPage++;
    final url = Uri.https(_url, '3/movie/popular',{
      'api_key': _apiKey,
      'language': _language,
      'page': _popularesPage.toString()
    });

    final response = await _procesarRespuesta(url);
    _populares.addAll(response);
    popularesSink(_populares);
    _cargando = false;
    return response;
  }

  Future<List<Pelicula>> buscarPeliculas(String query) async {
    final url = Uri.https(_url, '3/search/movie',{
      'api_key': _apiKey,
      'language': _language,
      'query' : query
    });

    return await _procesarRespuesta(url);

  }

}