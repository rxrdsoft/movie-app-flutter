import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movie_app/src/models/actores_model.dart';

class ActoresProvider {
  String _apiKey = '27e43c48a67719f0f5211724f785b4cd';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  Future<List<Actor>> _procesarRespuesta(Uri url) async {
    final response = await http.get(url);
    final decodedData = jsonDecode(response.body);
    final actores = Actores.fromJsonList(decodedData['cast']);
    return actores.items;
  }

  Future<List<Actor>> getActores(int movieId) async {
    final url = Uri.https(_url, '3/movie/$movieId/credits',{
      'api_key': _apiKey,
      'language': _language
    });
    final actores  = await _procesarRespuesta(url);
    print(actores);
    return await _procesarRespuesta(url);

  }
}